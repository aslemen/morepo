import sys
import click

import BTrees.IOBTree as iob

import typing

from . import dbio
from . import inputters as inp
from . import filters as fil
from . import objects as objs
WTL = fil.WTL
from . import outputters as outp

import transaction
"""
This module provides a router towards formatters (implemented as submodules),
which is comparable to the Controller module in the MVC model.
"""

# ======
# 0. Main
# ======
@click.group()
@click.argument("repository",
        type = click.Path(
            exists = False,
            file_okay = True,
            resolve_path = True))
@click.pass_context
def routine(context, repository):
    # Open the database
    conn = dbio.get_db_connection(repository)

    # Add the connection to the context
    context.connection = conn

    # Continue to subcommands
    # ===END===

# ======
# 1. Input (Insert)
# ======

INPUT_FORMAT = {
        "morep_json": inp.morep_json
        }

@routine.command("batchadd")
@click.argument("format",
        type = click.Choice(INPUT_FORMAT.keys()))
@click.option("--path", "-p",
        type = click.Path(
            exists = True,
            resolve_path = True),
        prompt = True)
@click.pass_context
def routine_batchadd(context, format, path):
    ipf = INPUT_FORMAT[format]

    records = ipf.convert_records_to_Morep_Base(
            records_raw = ipf.extract_from_files(
                                root_dir = path))

    # Print Logs to STDERR
    sys.stderr.write("====== LOG: morepo input add ======\n")
    sys.stderr.write("Record Key\t| Item\n")
    sys.stderr.write("--------------------------\n")

    tree_root = context.parent.connection.root.records
    for r in records:
        key, res = dbio.insert_new_record(
                tree_pointer = tree_root,
                record = r
                )
        sys.stderr.write("{0}\t| {1}\n".format(
            key, repr(res)))

    for key, val in tree_root.items():
        if isinstance(val, objs.Morep_Bib_Incollection):
            pointer = val.container

            if not isinstance(val.container, objs.Morep_Bib_Base):
                candidates = WTL.select_by_langs(
                    WTL.select_by_work_ids(
                        tree_root.items(),
                        [pointer["work_id"]]
                        ),
                    [pointer["lang"]]
                    )

                _, container = next(candidates)
                val.container = container

        transaction.commit()

    # ===END===

# ======
# 2. Delete
# ======
@routine.command("batchdelete")
@click.argument("record_keys",
        nargs = -1,
        type = click.INT
        )
@click.pass_context
def routine_batchdelete(context, record_keys):
    # Print Logs to STDERR
    sys.stderr.write("====== LOG: morepo input delete ======\n")
    sys.stderr.write("Record Key\t| Item\n")
    sys.stderr.write("--------------------------\n")

    for i in record_keys:
        key, res = dbio.pop_record(
                tree_pointer = context.parent.connection.root.records,
                ID = i
                )
        sys.stderr.write("{0}\t| {1}\n".format(
            key, repr(res)))

    # ===END===

# ======
# 3. Edit
# ======

# ======
# 3.1 Link a record to another's container
# ======


# ======
# 4. Select (Filter + Output)
# ======

@routine.group(chain = True, invoke_without_command = True, name = "select")
@click.pass_context
def routine_select(context): pass

@routine_select.resultcallback()
def routine_select_pipeline(processors):
    context = click.get_current_context()
    root = context.parent.connection.root
    records = root.records

    iterator = records.items()

    for processor in processors:
        iterator = processor(iterator)

    for item in iterator:
        print(str(item))

    # ===END===

# ======
# 4.1 Filter
# ======

@routine_select.command("through")
def routine_select_filter_WTsLmost():
    def processor(iterator):
        yield from fil.default.select_void(iterator)

    return processor
    # ===END===

@routine_select.command("filter_keys")
@click.option("--key", "-k",
        multiple = True,
        type = click.INT
        )
def routine_select_filter_record_keys(key: typing.List[int]):
    def processor(iterator):
        yield from fil.default.select_record_keys(
                records = iterator,
                record_keys = key
                )

    return processor
    # ===END===


#@routine_select.group(
#        chain = True,
#        invoke_without_command = True,
#        name = "filter")
#def routine_select_filter(): pass
#
#@routine_select_filter.resultcallback()
#def routine_select_pipeline(processors):
#    def whole_processors(iterator):
#        result_iterator = iterator
#        for processor in processors:
#            iterator = processor(iterator)
#
#        yield from result_iterator
#
#    return whole_processors

# @routine_select_filter.command("WTsLmost")
@routine_select.command("filter_language_most_by_ID")
@click.option("--language", "-l",
        multiple = True,
        type = click.STRING)
def routine_select_filter_language_most_by_ID(
        language: typing.List[str]
        ):
    def processor(iterator):
        yield from WTL.select_most_preferred_lang_for_each_work_id(
            records = iterator,
            langs_ordered = language
            )

    return processor
    # ===END===

@routine_select.command("filter_worktypes")
@click.option("--work_type", "-wt",
        multiple = True,
        type = click.STRING)
def routine_select_filter_work_types(
        work_type: typing.List[str]
        ):
    def processor(iterator):
        yield from WTL.select_by_work_types(
            records = iterator,
            work_types = work_type
            )

    return processor
    # ===END===

@routine_select.command("filter_languages")
@click.option("--language", "-l",
        multiple = True,
        type = click.STRING)
def routine_select_filter_language(
        language: typing.List[str]
        ):
    def processor(iterator):
        yield from WTL.select_by_langs(
            records = iterator,
            langs = language
            )

    return processor
    # ===END===

@routine_select.command("filter_IDs")
@click.option("--id", "-i",
        multiple = True,
        type = click.STRING)
def routine_select_filter_ids(
        id: typing.List[int]
        ):
    def processor(iterator):
        yield from WTL.select_by_work_ids(
            records = iterator,
            work_ids = id
            )

    return processor
    # ===END===

@routine_select.command("match_titles")
@click.option("--title", "-t",
        multiple = True,
        type = click.STRING)
def routine_select_filter_ids(
        title: typing.List[str]
        ):
    def processor(iterator):
        yield from WTL.select_match_titles(
            records = iterator,
            title_re_s = title
            )

    return processor
    # ===END===

# ======
# 4.2 Output
# ======

OUTPUT_FORMAT = {
        "raw": outp.raw,
        "APAlikeHTML": outp.APAlikeHTML
        }

@routine_select.command("output")
@click.argument("format",
        type = click.Choice(OUTPUT_FORMAT.keys()),
        default = "raw")
def routine_select_output(format):
    return OUTPUT_FORMAT[format].create_Outputters

# ======
# Execute the routine
# ======
routine(None)
