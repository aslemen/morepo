import json
import glob

import typing
import numbers
import packaging.version as vers

import morepo.objects as objs
import morepo.dbio as dbio
import morepo.filters.WTL as wtl

import BTrees.OOBTree as oob
import persistent.list as pl
import persistent.dict as pd

def extract_from_stream(stream) -> typing.List[dict]:
    """
        Extract morep.json data from a stream.

        Arguments
        ---------
        stream:
            A stream
    """
    jsdata: dict = json.load(stream)

    # check the version
    # ---
    if vers.Version(jsdata["version"]) < vers.Version("1.1"):
        raise Exception()

    res: typing.List[dict] = jsdata["content"]
    return res

    # END

def extract_from_files(root_dir: str, extension: str = "morep.json"):
    files_path = glob.iglob(root_dir + ".".join((r"**/*", extension)), recursive = True)

    res_list = []

    for fp in files_path:
        with open(fp) as stream:
            res_list.extend(extract_from_stream(stream))

    return res_list

JSON_TYPE_to_MOREP = {
        "book": objs.Morep_Bib_Book,
        "proceedings": objs.Morep_Bib_Proceedings,
        "journal": objs.Morep_Bib_Journal,
        "inbook": objs.Morep_Bib_Inbook,
        "inproceedings": objs.Morep_Bib_Inproceedings,
        "paper": objs.Morep_Bib_Paper,
        "presentation": objs.Morep_Bib_Presentation,
        "thesis": objs.Morep_Bib_Thesis
        }

def convert_list_to_PersistentList_recursively(item: typing.Any) -> pl.PersistentList:
    if isinstance(item, list):
        return pl.PersistentList(map(convert_list_to_PersistentList_recursively, item))
    else:
        return item

def convert_dict_to_OOBTree_recursively(item: typing.Any) -> oob.BTree:
    if isinstance(item, dict):
        res = oob.BTree()
        for k, v in item.items():
            res[k] = convert_dict_to_OOBTree_recursively(v)
    else:
        return item

def convert_subitems_to_Persistent_recursively(item: typing.Any):
    res = None

    if isinstance(item, numbers.Number) or isinstance(item, str) or isinstance(item, bool):
        res = item
    elif isinstance(item, list):
        res = pl.PersistentList()

        for subitem in item:
            res.append(convert_subitems_to_Persistent_recursively(subitem))

    elif isinstance(item, dict):
        res = pd.PersistentDict()

        for subitem in item.items():
            key, val = subitem

            res[convert_subitems_to_Persistent_recursively(key)] = \
                    convert_subitems_to_Persistent_recursively(val)
    else:
        res = str(item)

    return res

def convert_to_Morep_Base(record_raw: typing.Dict) -> objs.Morep_Bib_Base:
    entity = JSON_TYPE_to_MOREP[record_raw["work_type"]]()

    for k, v in record_raw.items():
        setattr(entity, k, convert_subitems_to_Persistent_recursively(v))

    return entity

def convert_records_to_Morep_Base(records_raw: typing.Iterator[dict]) -> typing.Iterator[objs.Morep_Bib_Base]:
    return map(convert_to_Morep_Base, records_raw)

