import persistent

class Morep_Bib_Base(persistent.Persistent):
    def __str__(self):
        return "<{typename};\twork id: {work_id},\tlang: {lang};\ttitle: {title}>".format(
                typename = self.__class__.__name__,
                work_id = self.work_id,
                lang = self.lang,
                title = self.title[0])

    def __repr__(self):
        return "<{typename}\tat {ident:x};\twork id: {work_id},\tlang: {lang};\ttitle: {title}>".format(
                typename = self.__class__.__name__,
                ident = id(self),
                work_id = self.work_id,
                lang = self.lang,
                title = self.title[0])

# ======
# Collection Classes
# ======
class Morep_Bib_Collection(Morep_Bib_Base):
    pass

class Morep_Bib_Book(Morep_Bib_Collection):
    pass

class Morep_Bib_Proceedings(Morep_Bib_Collection):
    pass

class Morep_Bib_Journal(Morep_Bib_Collection):
    pass

# ======
# In-Collection Classes
# ======
class Morep_Bib_Incollection(Morep_Bib_Base):
    pass

class Morep_Bib_Inbook(Morep_Bib_Incollection):
    pass

class Morep_Bib_Inproceedings(Morep_Bib_Incollection):
    pass

class Morep_Bib_Paper(Morep_Bib_Incollection):
    pass

# ======
# Others
# =======
class Morep_Bib_Thesis(Morep_Bib_Base):
    pass

class Morep_Bib_Presentation(Morep_Bib_Base):
    pass

