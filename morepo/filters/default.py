import morepo.objects as objs

import typing

def select_void(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]]
        ):
    return records

def select_record_keys(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        record_keys: typing.List[int]
        ):
    return filter(lambda item: item[0] in record_keys, records)
