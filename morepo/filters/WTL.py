import BTrees.IOBTree as iob
import BTrees.OOBTree as oob
import BTrees.OIBTree as oib

import morepo.objects as objs

import itertools
import typing

import re

WORK_TYPE_STR = {
        "proceedings": objs.Morep_Bib_Proceedings,
        "journal": objs.Morep_Bib_Journal,
        "book": objs.Morep_Bib_Book,
        "paper": objs.Morep_Bib_Paper,
        "inproceedings": objs.Morep_Bib_Inproceedings,
        "inbook": objs.Morep_Bib_Inbook,
        "presentation": objs.Morep_Bib_Presentation,
        "thesis": objs.Morep_Bib_Thesis
        }

def select_by_work_types(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        work_types: typing.List[str]):
    work_type_list = list(map(lambda x: WORK_TYPE_STR[x], work_types))

    return filter(
            lambda item: (type(item[1]) in work_type_list),
            records)

def select_by_work_ids(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        work_ids: typing.List[int]):
    return filter(
            lambda item: (item[1].work_id in work_ids),
            records)

def select_by_langs(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        langs: typing.List[str]):
    return filter(
            lambda item: (item[1].lang in langs),
            records)

def select_most_preferred_lang_for_each_work_id(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        langs_ordered: typing.List[str]):

    # ID順にsortして，第2に言語順にsortする
    recs = list(records)

    #recs_sorted = sorted(records,
    recs_sorted = sorted(
            sorted(
                recs,
                reverse = False,
                key = lambda x: langs_ordered.index(x[1].lang)
                ),
            key = lambda x: x[1].work_id,
            reverse = True
            )

    # 次にIDごとにリストを分割（uniqと同じ）
    recs_devided_by_work_id = map(
            lambda item: item[1],
            itertools.groupby(
                                recs_sorted, key = lambda x: x[1].work_id))

    # 言語に関して最大値を1つ取る
    recs_devided_by_work_id_selected_by_most_preferred_lang = \
            map(lambda sublist: itertools.islice(sublist, 1),
                    recs_devided_by_work_id)

    # それを潰したものを返す
    return itertools.chain.from_iterable(
            recs_devided_by_work_id_selected_by_most_preferred_lang)

def select_match_titles(
        records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]],
        title_re_s: typing.List[str]
        ):
    regex = re.compile("({res})".format(
        res = "|".join(map(
            lambda s: "({re})".format(re = str(s)),
            title_re_s
            ))
            )
        )

    return filter(
            lambda item: regex.match(" ".join(item[1].title)) is not None,
            records
            )
