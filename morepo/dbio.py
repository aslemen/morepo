import ZODB
import BTrees.IOBTree as iob
import morepo.objects as objs
import transaction

import typing
import copy
import atexit

def get_db_connection(db_path: str):
    # Obtain the connection to db_path
    db = ZODB.DB(db_path)
    conn = db.open()

    # Register the finalizer
    def close_conn(conn):
        transaction.abort()
        conn.close()
        db.close()

    atexit.register(lambda: close_conn(conn))

    # Initialization if necessary 
    if not hasattr(conn.root, "records"):
        conn.root.records = iob.BTree()

    # return the connection
    return conn

def insert_new_record(
        tree_pointer: iob.BTree,
        record: objs.Morep_Bib_Base,
        is_copied: bool = False) -> typing.Tuple[int, objs.Morep_Bib_Base]:
    record_copy = copy.deepcopy(record) if is_copied else record

    key = 0
    if len(tree_pointer) > 0:
        key = tree_pointer.maxKey() + 1

    res = (key, record_copy)

    try:
        tree_pointer.insert(key, record_copy)

        transaction.commit()
    finally:
        transaction.abort()

    return res

def pop_record(
        tree_pointer: iob.BTree,
        ID: int
        ) -> typing.Tuple[int, objs.Morep_Bib_Base]:
    try:
        res = (ID, tree_pointer.pop(ID))

        transaction.commit()

        return res
    finally:
        transaction.abort()


def get_all_records_from_connection(
            connection_pointer
                    ) -> typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]]:
    records = connection_pointer.root.records
    return records.items()
