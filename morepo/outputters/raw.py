import morepo.objects as objs

from collections import defaultdict
import typing

class Output_Base:
    def __init__(self, record: objs.Morep_Bib_Base):
        if isinstance(record, objs.Morep_Bib_Base):
            self.record = record
        else:
            raise TypeError()

    def print_all(self):
        return repr(self.record)

    # ===END===

class Output_Extension:
    def __init__(self, key: int, out: Output_Base):
        if isinstance(out, Output_Base):
            self.key = key
            self.output_record = out
        else:
            raise TypeError()

    def __repr__(self):
        return "<{0}, {1}>".format(
                str(self.key), repr(self.output_record)
                )

    def __str__(self):
        return "<{0}, {1}>".format(
                str(self.key), str(self.output_record)
                )

def find_Outputter(morep_record: objs.Morep_Bib_Base) -> Output_Base:
    return Output_Base(morep_record)

def create_Outputters(
        morep_records: typing.Iterator[typing.Tuple[int, objs.Morep_Bib_Base]]
            ) -> typing.Iterator[Output_Base]:

    for key, val in morep_records:
        yield Output_Extension(key, find_Outputter(val))
