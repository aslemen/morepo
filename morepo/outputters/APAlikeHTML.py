import datetime
import locale

import itertools
from collections import defaultdict
from abc import ABCMeta, abstractmethod

import morepo.objects as objs

import BTrees.IOBTree as iob

import typing

PERIOD = defaultdict(lambda: ". ")
PERIOD["ja"] = "．"

COMMA = defaultdict(lambda: ", ")
COMMA["ja"] = "，"

NAME_FAMILY_SEPARATOR = defaultdict(lambda: ", ")
NAME_FAMILY_SEPARATOR["ja"] = " "

NAME_MID_SEPARATOR = defaultdict(lambda: " ")
NAME_MID_SEPARATOR["ja"] = " "

TITLE_SEPARATOR = defaultdict(lambda: ": ")
TITLE_SEPARATOR["ja"] = "―"

PUBLOC_SEPARATOR = defaultdict(lambda: ": ")
PUBLOC_SEPARATOR["ja"] = "："

LINE_BREAK_HTML = {True: "<br />", False: ""}

PARENTHESIS_BEGIN = defaultdict(lambda: "(")
PARENTHESIS_BEGIN["ja"] = "（"

PARENTHESIS_END = defaultdict(lambda: ")")
PARENTHESIS_END["ja"] = "）"

INCLUDED_IN = defaultdict(lambda: "In: ")
INCLUDED_IN["ja"] = ""

def print_join_GENERAL(
        data_list: typing.List[str],
        separator: str,
        final_period: str = "",
        ) -> str:

    return separator.join(filter(bool, data_list)) + final_period
    # ======END======

def print_name_GENERAL(name: typing.List[str], lang: str) -> str:
    name_list = [
            "".join(name[0:1]),
             NAME_MID_SEPARATOR[lang].join(name[1:])]

    return print_join_GENERAL(
            data_list = name_list,
            separator = NAME_FAMILY_SEPARATOR[lang]
            )
# ======END======

def print_all_names_GENERAL(pre: typing.List[typing.List[str]] = [],
                                                main: typing.List[str] = [],
                                                post: typing.List[typing.List[str]] = [],
                                                lang: str = "en",
                                                bold_main_name:bool = True) -> str:
    # ======
    # 著者・発表者
    # ======

    # 本人よりも前のもの
    contribs_pre_str = map(
            lambda name: print_name_GENERAL(name, lang),
            pre
            )

    # 本人
    contrib_str = print_name_GENERAL(main, lang)
    if contrib_str and bold_main_name:
        contrib_str = "<b>" + contrib_str + "</b>"

    # 本人よりも後ろにあるもの
    contribs_post_str = map(
            lambda name: print_name_GENERAL(name, lang),
            post
            )

    contribs_all = itertools.chain(
            contribs_pre_str,
            filter(bool, [contrib_str]),
            contribs_post_str
            )

    contrib_CONJOINER = defaultdict(lambda: " &amp; ")
    contrib_CONJOINER["ja"] = COMMA["ja"]

    return contrib_CONJOINER[lang].join(contribs_all)
    # ======END======

class Output_Base(metaclass = ABCMeta):
    def __init__(self, record: objs.Morep_Bib_Base):
        if isinstance(record, objs.Morep_Bib_Base):
            self.record = record
        else:
            raise TypeError()

    def print_all_authors(self) -> str:
        return print_all_names_GENERAL(
                pre = getattr(self.record, "authors_pre", []),
                main = self.record.author,
                post = getattr(self.record, "authors_post", []),
                lang = self.record.lang)
        # ======END======

    def print_all_editors(self) -> str:
        editors_seq: str = print_all_names_GENERAL(
                pre = getattr(self.record, "editors_pre", []),
                main = self.record.editor,
                post = getattr(self.record, "editors_post", []),
                lang = self.record.lang)

        EDITED = defaultdict(lambda: " (Ed.)")
        EDITED = defaultdict(lambda: " (Hrsg.)")
        EDITED["ja"] = "（編）"

        return editors_seq + EDITED[self.record.lang]
        # ======END======

    def print_all_contributers_adequately(self) -> str:
        whether_to_show_authors = getattr(self.record, "show_authors", True) and hasattr(self.record, "author")

        whether_to_show_editors = getattr(self.record, "show_authors", True) and (
                hasattr(self.record, "editor") or \
                        hasattr(self.record, "editors_pre")
                        )

        res: typing.List[str] = []

        if whether_to_show_editors:
            res += [self.print_all_editors()]

        if whether_to_show_authors:
            res += [self.print_all_authors()]

        return print_join_GENERAL(
                data_list = res,
                separator = PERIOD[self.record.lang]
                )
        # ======END======

    def print_refereed(self) -> str:
        REFEREED_TEXT = defaultdict(lambda: "refereed")
        REFEREED_TEXT["ja"] = "査読あり"
        REFEREED_TEXT["de"] = "begutachtet"

        res: str = ""

        if hasattr(self.record, "refereed"):
            res = "<span class=\"work_refereed\">{0}</span>".format(
                    REFEREED_TEXT[self.record.lang]
                    ) if self.record.refereed else ""
            # ======END IF======

        return res
        # ======END======

    NO_DATE = defaultdict(lambda: "(n. d.)")
    NO_DATE["ja"] = "不明"

    def print_year(self):
        if hasattr(self.record, "date_begin"):
            return PARENTHESIS_BEGIN[self.record.lang] + \
                            str(self.record.date_begin["year"]) + \
                                    PARENTHESIS_END[self.record.lang]
        else:
            return self.NO_DATE[self.record.lang]
        # ======END======

    @staticmethod
    def print_date_base_GENERAL(dat: datetime.date, lang: str) -> str:
        DATE_PRESENTATION_STRFT_LANG = defaultdict(lambda: "(%b %-d, %Y)")
        DATE_PRESENTATION_STRFT_LANG["ja"] = "（%Y年%-m月%-d日）"
        DATE_PRESENTATION_STRFT_LANG["de"] =  "(%-d. %B, %Y)"

        DATE_PRESENTATION_LOCALE = defaultdict(lambda: "en_US.UTF-8")
        DATE_PRESENTATION_LOCALE["ja"] = "ja_JP.UTF-8"
        DATE_PRESENTATION_LOCALE["de"] = "de_DE.UTF-8"

        current_locale = locale.getlocale(locale.LC_TIME)

        locale.setlocale(locale.LC_TIME, DATE_PRESENTATION_LOCALE[lang])

        res = dat.strftime(DATE_PRESENTATION_STRFT_LANG[lang])

        locale.setlocale(locale.LC_TIME, current_locale)

        return res
        # ======END======

    def print_date(self) -> str:
        if hasattr(self.record, "date_begin"):
            if self.record.date_begin is not None:
                return self.print_date_base_GENERAL(
                    dat = datetime.date(
                        self.record.date_begin["year"],
                        self.record.date_begin["month"],
                        self.record.date_begin["day"]),
                    lang = self.record.lang)

        return self.NO_DATE[self.record.lang]
        # ======END======

    def print_title(self, is_bold: bool = False):
        res = TITLE_SEPARATOR[self.record.lang].join(self.record.title)
        wrapper = "<b>{0}</b>" if is_bold else "{0}"

        return wrapper.format(res)
        # ======END======

    def print_pages(self):
        PAGE = defaultdict(lambda: "p.")
        PAGE["de"] = "S."

        PAGES = defaultdict(lambda: "pp.")
        PAGES["de"] = "S."

        begin = self.record.pages["begin"]
        end = self.record.pages["end"]

        res: str = None

        if begin == end:
            res = PARENTHESIS_BEGIN[self.record.lang] + \
                    "{page} {begin}".format(
                        page = PAGE[self.record.lang],
                        begin = self.record.pages["begin"],
                        ) + \
                    PARENTHESIS_END[self.record.lang]
        else:
            res = PARENTHESIS_BEGIN[self.record.lang] + \
                    "{page} {begin}&ndash;{end}".format(
                        page = PAGE[self.record.lang],
                        begin = self.record.pages["begin"],
                        end = self.record.pages["end"]
                        ) + \
                    PARENTHESIS_END[self.record.lang]

        return res
        # ===END===

    def print_location(self):
        return COMMA[self.record.lang].join(self.record.location)
        # ======END======

    @abstractmethod
    def print_all(self,
            is_with_linebreak: bool = True,
            whether_to_show_date: bool = False,
            pages: typing.List[str] = None):
        pass

    def __str__(self) -> str:
        return self.print_all(is_with_linebreak = True)

    # ======END CLASS======

class Output_Collection(Output_Base, metaclass = ABCMeta):
    def __init__(self, record: objs.Morep_Bib_Collection):
        if isinstance(record, objs.Morep_Bib_Collection):
            super().__init__(record)
        else:
            raise TypeError()

    def print_series(self):
        if hasattr(self.record, "series"):
            return COMMA[self.record.lang].join(
                self.record.series)
        else:
            return ""

    def print_pub_info(self):
        location = COMMA[self.record.lang].join(
                self.record.location)
        publisher = COMMA[self.record.lang].join(
                self.record.publisher)

        if self.record.lang == "ja":
            return publisher
        else:
            return print_join_GENERAL(
                    data_list = [location, publisher],
                    separator = TITLE_SEPARATOR[self.record.lang],
                    final_period = ""
                    )
        # ===END===

    def print_title(self, is_bold: bool = False):
        res_raw = super().print_title(is_bold = False)
        res = ("『" + res_raw + "』") \
                if self.record.lang == "ja" \
                else ("<i>" + res_raw + "</i>")

        wrapper = "<b>{0}</b>" if is_bold else "{0}"

        return wrapper.format(res)

    def print_all(self,
            is_with_linebreak: bool = True):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_year(),
                self.print_title(is_bold = True),
                self.print_series(),
                self.print_pub_info()
                ]

        return print_join_GENERAL(
                data_list = res_list,
                separator = sep,
                final_period = PERIOD[self.record.lang]
                )
        # ======END======

    def print_all_included(self,
            is_with_linebreak: bool = False,
            whether_to_show_date: bool = False,
            pages: str = None):
        title_series = print_join_GENERAL(
                data_list = [self.print_title(is_bold = False), self.print_series()],
                separator = COMMA[self.record.lang]
                )

        TITLE_PAGE_SPACE = defaultdict(lambda: " ")
        TITLE_PAGE_SPACE["ja"] = ""

        title_series_pages = print_join_GENERAL(
                data_list = [title_series, pages],
                separator = TITLE_PAGE_SPACE[self.record.lang]
                )

        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_year() if whether_to_show_date else "",
                title_series_pages,
                self.print_pub_info()
                ]

        return print_join_GENERAL(
                data_list = res_list,
                separator = sep
                )
        # ======END======

Output_Book = Output_Collection

Output_Proceedings = Output_Collection

class Output_Journal(Output_Collection):
    def __init__(self, record: objs.Morep_Bib_Journal):
        if isinstance(record, objs.Morep_Bib_Journal):
            super().__init__(record)
        else:
            raise TypeError()

    def print_all_included_journal(self,
            is_with_linebreak: bool = False,
            whether_to_show_date: bool = False,
            volume_issue: str = None,
            pages: str = None):

        volume_issues_pages_list = [
                volume_issue,
                pages
                ]

        volume_issues_pages_str = print_join_GENERAL(
                data_list = volume_issues_pages_list,
                separator = COMMA[self.record.lang]
                )

        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
                self.print_title(is_bold = False),
                volume_issues_pages_str
                ]

        return print_join_GENERAL(
                data_list = res_list,
                separator = sep,
                final_period = PERIOD[self.record.lang]
                )
        # ======END======
    # ======END CLASS======

class Output_Incollection(Output_Base, metaclass = ABCMeta):
    def __init__(self, record: objs.Morep_Bib_Incollection):
        if isinstance(record, objs.Morep_Bib_Incollection):
            super().__init__(record)
        else:
            raise typeerror()

    @abstractmethod
    def print_all(self, is_with_linebreak: bool = True):
        pass
    # ======end class======

class Output_Inbook(Output_Incollection):
    def __init__(self, record: objs.Morep_Bib_Inbook):
        if isinstance(record, objs.Morep_Bib_Inbook):
            super().__init__(record)
        else:
            raise typeerror()

    def print_all(self, is_with_linebreak: bool = True):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
            self.print_all_contributers_adequately(),
            self.print_year(),
            self.print_title(is_bold = True),
            INCLUDED_IN[self.record.lang] + \
                Output_Book(
                    self.record.container
                    ).print_all_included(
                        is_with_linebreak = False,
                        pages = self.print_pages()
                        )
            ]

        return print_join_GENERAL(
            data_list = res_list,
            separator = sep,
            final_period = PERIOD[self.record.lang]
            )
        # ======END======
    # ======END CLASS======

class Output_Inproceedings(Output_Incollection):
    def __init__(self, record: objs.Morep_Bib_Inproceedings):
        if isinstance(record, objs.Morep_Bib_Inproceedings):
            super().__init__(record)
        else:
            raise TypeError()

    def print_all(self, is_with_linebreak: bool = True):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        refereed = self.print_refereed()

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_year(),
                self.print_title(is_bold = True),
                INCLUDED_IN[self.record.lang] + \
                    Output_Proceedings(
                        self.record.container
                        ).print_all_included(
                            is_with_linebreak = False,
                            pages = self.print_pages()
                            )
                ]

        return print_join_GENERAL(
                data_list = [
                    refereed,
                    print_join_GENERAL(
                        data_list = res_list,
                        separator = sep,
                        final_period = PERIOD[self.record.lang]
                        )
                    ],
                separator = " ",
                final_period = ""
                )
        # ======END======
    # ======END CLASS======

class Output_Paper(Output_Incollection):
    def __init__(self, record: objs.Morep_Bib_Paper):
        if isinstance(record, objs.Morep_Bib_Paper):
            super().__init__(record)
        else:
            raise TypeError()

    def print_volume_issue(self):
        res_list = []

        if hasattr(self.record, "volume"):
            res_list += [str(self.record.volume)]

        if hasattr(self.record, "issue"):
            res_list += ["({0})".format(str(self.record.issue))]

        return print_join_GENERAL(
            data_list = res_list,
            separator = ""
            )

    def print_all(self,
            is_with_linebreak: bool = True,
            whether_to_show_date: bool = False):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_year(),
                self.print_title(is_bold = True),
                Output_Journal(
                    self.record.container
                    ).print_all_included_journal(
                        volume_issue = self.print_volume_issue(),
                        pages = self.print_pages()
                        )
                    ]

        return print_join_GENERAL(
                data_list = [
                    refereed,
                    print_join_GENERAL(
                        data_list = res_list,
                        separator = sep,
                        final_period = PERIOD[self.record.lang]
                        )
                    ],
                separator = " ",
                final_period = ""
                )
        # ======END======
    # ======END CLASS======

class Output_Presentation(Output_Base):
    def __init__(self, record: objs.Morep_Bib_Presentation):
        if isinstance(record, objs.Morep_Bib_Presentation):
            super().__init__(record)
        else:
            raise TypeError()

    def print_conference(self):
        return TITLE_SEPARATOR[self.record.lang].join(self.record.conference)
        # ======END======

    def print_all(self,
            is_with_linebreak: bool = True):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        refereed = self.print_refereed()

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_date(),
                self.print_title(is_bold = True),
                self.print_conference(),
                self.print_location()
                ]

        return print_join_GENERAL(
                data_list = [
                    refereed,
                    print_join_GENERAL(
                        data_list = res_list,
                        separator = sep,
                        final_period = PERIOD[self.record.lang]
                        )
                    ],
                separator = " ",
                final_period = ""
                )

        # ======END======
    # ======END CLASS======

class Output_Thesis(Output_Base):
    def __init__(self, record: objs.Morep_Bib_Thesis):
        if isinstance(record, objs.Morep_Bib_Thesis):
            super().__init__(record)
        else:
            raise TypeError()

    def print_thesis_type(self):
        return COMMA[self.record.lang].join(self.record.thesis_type)

    def print_institution(self):
        return COMMA[self.record.lang].join(self.record.institution)

    def print_all(self, is_with_linebreak: bool = True):
        sep = PERIOD[self.record.lang] + LINE_BREAK_HTML[is_with_linebreak]

        res_list = [
                self.print_all_contributers_adequately(),
                self.print_year(),
                self.print_title(is_bold = True),
                self.print_thesis_type(),
                self.print_institution()
                ]

        return print_join_GENERAL(
                data_list = res_list,
                separator = sep,
                final_period = PERIOD[self.record.lang]
                )
        # ======END======
    # ======END CLASS======

# ======

class Output_Extension:
    def __init__(self, keyval):
        INIT_MAPPER = {
            objs.Morep_Bib_Book: Output_Book,
            objs.Morep_Bib_Proceedings: Output_Proceedings,
            objs.Morep_Bib_Inbook: Output_Inbook,
            objs.Morep_Bib_Inproceedings: Output_Inproceedings,
            objs.Morep_Bib_Paper: Output_Paper,
            objs.Morep_Bib_Presentation: Output_Presentation,
            objs.Morep_Bib_Thesis: Output_Thesis
            }
        key, record = keyval

        self.key = key
        self.record = INIT_MAPPER[type(record)](record)

    def __repr__(self):
        return "<{0}, {1}>".format(
                str(self.key), repr(self.record)
                )

    def __str__(self):
        return "<li>{0}</li>".format(
                self.record.print_all())

def create_Outputters(morep_records: iob.BTree) -> typing.Iterator[Output_Base]:
    return itertools.chain(["<ul>"], map(Output_Extension, morep_records), ["</ul>"])
