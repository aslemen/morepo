from setuptools import setup

setup(
        name = "morepo",
        version = "0.1",
        author = "aslemen",
        author_email = "net@hayashi-lin.net",
        packages = ["morepo"],
        install_requires = [
            "ZODB",
            "BTrees",
            "click"
            ],
        entry_points = """
        [console_scripts]
        morepo = morepo.__main__:routine
        """
        )
