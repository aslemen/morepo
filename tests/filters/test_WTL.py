import ZODB
import transaction
import BTrees.IOBTree as iob

import morepo.filters.WTL as wtl

import pytest

@pytest.mark.parametrize("work_types", [
            ["proceedings", "journal"],
            ["inbook", "inproceedings"],
            ["paper"],
            ["presentation"],
            ["presentation", "thesis"]
            ])
def test_filter_select_by_work_types(work_types, database_root):
    recs = database_root.records.items()

    results = wtl.select_by_work_types(recs, work_types)

    print("======")
    print("work types: [{0}]".format(", ".join(work_types)))
    for key, res in results:
        print("<{0}, {1}>".format(key, repr(res)))

    print("======")

@pytest.mark.parametrize("work_ids", [
            [20180527], [20170127, 20160118]])
def test_filter_select_by_work_ids(work_ids, database_root):
    recs = database_root.records.items()

    results = wtl.select_by_work_ids(recs, work_ids)

    print("======")
    print("work ids: [{0}]".format(", ".join(map(str, work_ids))))

    for key, res in results:
        print("<{0}, {1}>".format(key, repr(res)))

    print("======")

@pytest.mark.parametrize("langs", [
            ["en"], ["ja", "en"]])
def test_filter_select_by_langs(langs, database_root):
    recs = database_root.records.items()

    results = wtl.select_by_langs(recs, langs)

    print("======")
    print("languages: {0}".format(", ".join(map(str, langs))))

    for key, res in results:
        print("<{0}, {1}>".format(key, repr(res)))

    print("======")

@pytest.mark.parametrize("lang_order", [
            ["en", "ja"], ["ja", "en"]])
def test_filter_select_most_preferred_lang_for_each_work_id(
        lang_order,
        database_root):
    recs = database_root.records.items()

    results = wtl.select_most_preferred_lang_for_each_work_id(recs, lang_order)

    print("======")
    print("languages order: {0}".format(
        str(lang_order)))

    print(type(results))

    for key, res in results:
        print("<{0}, {1}>".format(key, repr(res)))

    print("======")
