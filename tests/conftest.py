import pytest
import os
import pathlib

import ZODB
import transaction
import BTrees.IOBTree as iob

import morepo.inputters.morep_json as mj
import morepo.dbio as dbio

@pytest.fixture(scope = "session")
def recs_test(request):
    FILE_PATH = "/home/owner/src/www/data/wksrc/"

    records_json = mj.extract_from_files(root_dir = FILE_PATH)

    records_morep = mj.convert_records_to_Morep_Base(
            records_json)

    return records_morep

@pytest.fixture(scope = "session")
def database_root(request, recs_test):
    # Configure the test database
    PATH = pathlib.Path("/tmp/morep_test_db.fs")

    if PATH.exists():
        os.remove(str(PATH))

    db = ZODB.DB(str(PATH))
    conn = db.open()
    root = conn.root

    root.records = iob.BTree()

    # Add Test Records
    print("=== INFO OF TEST DATABASE AND RECORDS ===")

    for r in recs_test:
        key, _ = dbio.insert_new_record(
            root.records,
            record = r)

        print("<{0}, {1}>".format(key, repr(r)))

    # Finalizer
    def disconnect():
        transaction.abort()
        conn.close()
        db.close()

    request.addfinalizer(disconnect)

    # Provide the database root
    return root
