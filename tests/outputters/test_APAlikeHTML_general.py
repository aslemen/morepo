import morepo.outputters.APAlikeHTML as outs
import typing
import pytest

@pytest.mark.parametrize(['name', 'lang', 'expected'],
        [(['Johnson', 'John'], "en", "Johnson, John"),
            (['Johnson', 'John', 'F.', 'K.'], "de", "Johnson, John F. K."),
            (['ジョンソン中村', 'あいう子'], "de", "ジョンソン中村, あいう子"),
            (['ジョンソン中村', 'エミリー', 'あいう子'], "ja", "ジョンソン中村 エミリー あいう子")])
def test_name(name: typing.List[str], lang: str, expected: str):
    assert outs.print_name_GENERAL(name, lang) == expected

ALL_CONTRIBUTERS = [
        ([], ["Johnson", "John", "F.", "K."], [], "en", "<b>Johnson, John F. K.</b>"),
        ([["Johnson", "John", "F.", "K."]], ["Hayashi", "T.", "N."], [["Abbey", "Jane"]], "en",
                            "Johnson, John F. K. &amp; <b>Hayashi, T. N.</b> &amp; Abbey, Jane"),
        ([], ['ジョンソン中村', 'エミリー', 'あいう子'], [], "ja",
            "<b>ジョンソン中村 エミリー あいう子</b>"),
        ([["狩野", "野心"], ["崎山", "仁友"]], ['ジョンソン中村', 'エミリー', 'あいう子'], [["戸井", "正和"]], "ja",
            "狩野 野心，崎山 仁友，<b>ジョンソン中村 エミリー あいう子</b>，戸井 正和")]

@pytest.mark.parametrize(['pre', 'main', 'post', 'lang', 'expected'], ALL_CONTRIBUTERS)
def test_all_contributers(pre, main, post, lang, expected):
    assert outs.print_all_names_GENERAL(pre, main, post, lang) == expected
