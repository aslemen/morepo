import morepo.objects as objs
import morepo.outputters as outs
import datetime

# ======
# test 2: Book
# ======
TEST_BOOK = objs.Morep_Bib_Book()
TEST_BOOK.authors_pre = []
TEST_BOOK.date_begin = {"year": 2030}
TEST_BOOK.author = ["Chimpsky", "Nim"]
TEST_BOOK.authors_post = [["Chomsky", "Noam", "Avram"]]
TEST_BOOK.title = ["A TEST OF BAKA", "An analysis of Japanese"]
TEST_BOOK.publisher = "MIT Press"
TEST_BOOK.location = ["Cambridge", "Massachusetts"]
TEST_BOOK.lang = "en"

def test_APAlikeHTML_book():
    assert str(outs.APAlikeHTML.Output_Book(TEST_BOOK)) == \
        r'<b>Chimpsky, Nim</b> &amp; Chomsky, Noam Avram. <br />(2030). <br /><i>A TEST OF BAKA: An analysis of Japanese</i>. <br />MIT Press: Cambridge, Massachusetts. '

# ======
# test 3: Proceedings
# ======
TEST_PROC = objs.Morep_Bib_Proceedings()
TEST_PROC.date_begin = {"year": 2030}
TEST_PROC.editors_pre = [["Coco"]]
TEST_PROC.editor = ["Chimpsky", "Nim"]
TEST_PROC.editors_post = [["Chomsky", "Noam", "Avram"]]
TEST_PROC.title = ["A TEST OF BAKA", "the 33rd Conference On Japanese Idiots"]
TEST_PROC.publisher = "MIT Press"
TEST_PROC.location = ["Cambridge", "Massachusetts"]
TEST_PROC.lang = "en"

def test_APAlikeHTML_proceedings():
    assert str(outs.APAlikeHTML.Output_Proceedings(TEST_PROC)) == \
            r'Coco &amp; <b>Chimpsky, Nim</b> &amp; Chomsky, Noam Avram (Ed.). <br />(2030). <br /><i>A TEST OF BAKA: the 33rd Conference On Japanese Idiots</i>. <br />MIT Press: Cambridge, Massachusetts. '

# ======
# test 4: Paper in Proceedings
# ======
TEST_INPROC = objs.Morep_Bib_Inproceedings()
TEST_INPROC.date_begin = {"year": 2030}
TEST_INPROC.authors_pre = []
TEST_INPROC.author = ["Chimpsky", "Nim"]
TEST_INPROC.authors_post = []
TEST_INPROC.title = ["Testing BAGA in German and Norwegian"]
TEST_INPROC.pages = {"begin": 223, "end": 345}
TEST_INPROC.location = ["Cambridge", "Massachusetts"]
TEST_INPROC.lang = "en"
TEST_INPROC.collection = TEST_PROC

def test_APAlikeHTML_inproceedings():
    assert str(outs.APAlikeHTML.Output_Inproceedings(TEST_INPROC)) == \
            r'<b>Chimpsky, Nim</b>. <br />(2030). <br />Testing BAGA in German and Norwegian. <br />Coco &amp; <b>Chimpsky, Nim</b> &amp; Chomsky, Noam Avram (Ed.). <i>A TEST OF BAKA: the 33rd Conference On Japanese Idiots</i>. 223 &endash; 345. MIT Press: Cambridge, Massachusetts. '

# ======
# test 5: Section in a book
# ======
TEST_INBOOK = objs.Morep_Bib_Inbook()
TEST_INBOOK.date_begin = {"year": 2030}
TEST_INBOOK.authors_pre = []
TEST_INBOOK.author = ["Coco"]
TEST_INBOOK.authors_post = []
TEST_INBOOK.title = ["Being Containted in a book"]
TEST_INBOOK.pages = {"begin": 22, "end": 34}
TEST_INBOOK.location = ["Tromsø", "Troms", "Norway"]
TEST_INBOOK.lang = "en"
TEST_INBOOK.collection = TEST_BOOK

def test_APAlikeHTML_inbook():
    assert str(outs.APAlikeHTML.Output_Inbook(TEST_INBOOK)) == \
            r'<b>Coco</b>. <br />(2030). <br />Being Containted in a book. <br /><b>Chimpsky, Nim</b> &amp; Chomsky, Noam Avram. <i>A TEST OF BAKA: An analysis of Japanese</i>. 22 &endash; 34. MIT Press: Cambridge, Massachusetts. '

# ======
# test 5-1: Journal
# ======
TEST_JOURNAL = objs.Morep_Bib_Journal()
TEST_JOURNAL.editors_pre = []
TEST_JOURNAL.editor = ["Johnson", "Samuel"]
TEST_JOURNAL.editors_post = []
TEST_JOURNAL.title = ["Linguistic Inquiry"]
TEST_JOURNAL.publisher = "MIT Press"
TEST_JOURNAL.location = ["Cambridge", "Massachusetts"]
TEST_JOURNAL.lang = "en"

def test_APAlikeHTML_journal():
    assert str(outs.APAlikeHTML.Output_Journal(TEST_JOURNAL)) == \
            r'<b>Johnson, Samuel</b> (Ed.). <br />(n. d.). <br /><i>Linguistic Inquiry</i>. <br />MIT Press: Cambridge, Massachusetts. '


# ======
# test 5-2: Paper
# ======
TEST_PAPER = objs.Morep_Bib_Paper()
TEST_PAPER.refereed = True
TEST_PAPER.authors_pre = []
TEST_PAPER.date_begin = {"year": 2009}
TEST_PAPER.author = ["Chimpsky", "Nim"]
TEST_PAPER.authors_post = [["Chomsky", "Noam", "Avram"]]
TEST_PAPER.title = ["A TEST OF BAKA", "An analysis of Japanese"]
TEST_PAPER.collection = TEST_JOURNAL
TEST_PAPER.volume = 12
TEST_PAPER.issue = 4
TEST_PAPER.pages = {"begin": 334, "end": 339}
TEST_PAPER.lang = "en"

def test_APAlikeHTML_paper():
    assert str(outs.APAlikeHTML.Output_Paper(TEST_PAPER)) == \
            r'<span class="work_refereed">refereed</span>. <br /><b>Chimpsky, Nim</b> &amp; Chomsky, Noam Avram. <br />A TEST OF BAKA: An analysis of Japanese. <br /><i>Linguistic Inquiry</i>. 12(4), 334 &endash; 339. '

# ======
# test 6: Presentation
# ======
TEST_PRESEN = objs.Morep_Bib_Presentation()
TEST_PRESEN.date_begin = {"year": 2013, "month": 9, "day": 10}
TEST_PRESEN.authors_pre = [["Coco"]]
TEST_PRESEN.author = ["Chimpsky", "Nim"]
TEST_PRESEN.authors_post = []
TEST_PRESEN.title = ["Speaking of BAKA in Japanese"]
TEST_PRESEN.conference = ["Workshop of idiots", "JGG 2018"]
TEST_PRESEN.location = ["University of Tokyo", "Komaba"]
TEST_PRESEN.lang = "en"
TEST_PRESEN.collection = TEST_BOOK

def test_APAlikeHTML_presentation():
    assert str(outs.APAlikeHTML.Output_Presentation(TEST_PRESEN)) == \
            r'Coco &amp; <b>Chimpsky, Nim</b>. <br />(10 Sep, 2013). <br />Speaking of BAKA in Japanese. <br />University of Tokyo: Komaba. <br />University of Tokyo, Komaba. '

# ======
# test 7: Thesis
# ======
TEST_THESIS = objs.Morep_Bib_Thesis()
TEST_THESIS.date_begin = {"year": 2013, "month": 9, "day": 10}
TEST_THESIS.authors_pre = []
TEST_THESIS.author = ["Chimpsky", "Nim"]
TEST_THESIS.authors_post = []
TEST_THESIS.title = ["A study of BAKA"]
TEST_THESIS.thesis_type = "Unpublished MA Thesis"
TEST_THESIS.institution = ["Department of Linguistics", "University of Mars"]
TEST_THESIS.lang = "en"

def test_APAlikeHTML_thesis():
    assert str(outs.APAlikeHTML.Output_Thesis(TEST_THESIS)) == \
            r'<b>Chimpsky, Nim</b>. <br />(2013). <br />A study of BAKA. <br />Unpublished MA Thesis. <br />Department of Linguistics, University of Mars. '


