import ZODB
import transaction
import atexit

root = None
recs = None

def open_db(path):
    global root
    global recs

    db = ZODB.DB(path)
    conn = db.open()

    root = conn.root

    if hasattr(root, "records"):
        recs = root.records

    def finalize():
        transaction.abort()
        conn.close()
        db.close()

    atexit.register(finalize)

    # ===END===
