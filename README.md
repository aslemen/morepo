# morepo: the REPOsitory of academic records for the website of MOri group
## 目的
[研究室ホームページ](http://phiz.c.u-tokyo.ac.jp/~morisem/)のための，業績情報管理・出力を行う．
Python 3で書かれていて，メンテナンスは比較的簡単である．

## 依存
このパッケージは，python 3で書かれている．version 3.6.5で動作確認をした．

このパッケージは，以下のパッケージに依存する：

- pip
- ZODB
- BTrees
- click

## インストール
以下は，暫定的なインストール方法である．

1. リポジトリをクローンする．
1. クローンしたリポジトリのルートフォルダに入る．
1. `python(3) setup.py develop`

## アンインストール
以下は，暫定的なアンインストール方法である．上のインストール法に従っているものとする．

1. `python(3) setup.py develop -u`


## 構成
MVCパターンに従う．

### Model
`morepo.objects`で定義されている．

### View (Selector) for Output
これは，フィルターと出力モジュールの2つに分割できる．

#### フィルター
`morepo.filters`で定義されている．

#### 出力モジュール	
`morepo.outputters`で定義されている．
出力モジュールとして，

- `raw`
- `APAlikeHTML`（APA風の，HTMLのための出力形式）

がある．

### Inputter
`morepo.inputters`で定義されている．
入力フォーマットとして，

- `morep_json`（下記参照）

に対応している．bibtexへの対応は将来の目標である．

出力先は必ず指定される．ZODBデータベース形式である．

### Controller
`morepo.__main__`で定義されている．`morepo.dbio`はヘルパーモジュールである．

読み込み・書き込み先は必ず指定される．ZODBデータベース形式である．

#### コマンドの構造 
```sh
python3 -m morepo <path_to_repository> <subcommand>
```

# 用法
## Insert
```sh
python3 -m morepo <path_to_repository> batchadd <format> --path <path_to_the_root_dir>
```

例：

```sh
python3 -m morepo temp.fs batchadd morep_json --path ~/src/work_list/ 
```

## Delete
```sh
python3 -m morepo <path_to_repository> batchdelete [record_key_1, [record_key_2, ...]]
```

例：

```sh
python3 -m morepo temp.fs batchdelete 23 25 27 
```

## Select
```sh
python3 -m morepo <path_to_repository> select (<filter_name_1> [filter_option_1])* output <format>
```

例：口頭発表の一覧を出力する．言語はイタリア語を優先するが，なければ，代替として英語のものが選択される．
最終的な出力は，APA風の，ウェブページ上で見栄えがいいようなもの（APAlikeHTML）をその形式とする．

```sh
python3 -m morepo temp.fs select filter_worktypes -wt presentation filter_language_most_by_ID -l it -l en output APAlikeHTML
```

# morep.json設定ファイル
以下の情報は古いかもしれません．

## 学位論文
```javascript
{
	"version": "1.1", 		// バージョン情報
	"content": [ 			// 中身
		{
			"work_type": "thesis", 	// 業績タイプ（必須）
			"lang": "en", 		// 文献表示言語（必須）
			"work_id": 20160118,		// レコード番号（必須，これによって，同一業績の言語のバリエーションを認識する．また，リストの順序もこれで決定される（降順）．
			"author": ["Hayashi", "T. N."], // 著者（必須）．姓，名の順．
			"date_begin": { 		// 認可の日付
				"year": 2016,
				"month": 1,
				"day": 18,
				"hour": 0,
				"minute": 0,
				"second": 0,
				"zone": "JST"
			},
			"date_end": { 			// 認可終了の日付（使われることはない）
				"year": 2016,
				"month": 1,
				"day": 18,
				"hour": 0,
				"minute": 0,
				"second": 0,
				"zone": "JST"
			},
			"title": ["Minimality Effects in Discourse", 
				"An SDRT Analysis of the Anaphoric Expressions <i>Zibun</i> and <i>the Zero Pronoun</i> in Japanese"],
				// 題名（必須）．リスト型である．複数の構成要素は，適当な記号で
				結合される（英語では": "）
			"thesis_type": ["Unpublished B.A. thesis"],
				// 論文の性質．これもリスト型である．
			"institution": ["College of Arts and Sciences",
					"the University of Tokyo"]
					// 大学，専攻の名
		},
		{ ... }
    ]
}
```

## 口頭発表
```javascript
{
	"version": "1.1",
	"content": [
		{
			"work_type": "presentation",
			"lang": "en",
			"speech_lang": "en",		\\ 発表言語
			"work_id": 20170127,
			"authors_pre":[["Moro", "ABCDE"], ["Abe", "Abebenobe"]],  \\ 共著者（前半）
			"author": ["Hayashi", "T. N."],
			"authors_post":[["Abebenobe", "ABBB"]],  		\\ 共著者（後半）
			"date_begin": { 		\\ 発表開始の日付
				"year": 2017,
				"month": 1,
				"day": 27,
				"hour": 12,
				"minute": 30,
				"second": 0,
				"zone": "CET" 
			},
			"date_end": { 			\\ 発表終了の日付
				"year": 2017,
				"month": 1,
				"day": 27,
				"hour": 13,
				"minute": 30,
				"second": 0,
				"zone": "CET" 
			},
			"title": ["Logophoric Anaphor and Empathy"],
			"conference": [
				"2. Workshop Germanistische Linguistik zwischen Köln und Tokio"
			],
			"refereed": false, \\査読つきか否か（デフォルトでfalse）
			"location": ["Universität zu Köln"]
		}
	]
}
```
